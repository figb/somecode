package com.yan.springrabbitmq.pojo;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private String name;
    private String id;

    private Date birth;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", birth=" + birth +
                '}';
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public User(String name, String id, Date birth) {

        this.name = name;
        this.id = id;
        this.birth = birth;
    }

}
