package com.yan.producer;

import com.yan.producer.service.UserServiceImpl;
import com.yan.producer.util.MyRegistry;
import com.yan.service.UserService;

//@SpringBootApplication
public class ProducerApplication {

    public static void main(String[] args) {
//        SpringApplication.run(ProducerApplication.class, args);
        MyRegistry.putService(UserService.class, UserServiceImpl.class);//预先放入可供客户端远程调用的接口
        MyRegistry myRegistry = new MyRegistry();
        try {
            myRegistry.Listen();//开启监听
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
