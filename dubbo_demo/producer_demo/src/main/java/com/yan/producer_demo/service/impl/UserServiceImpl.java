package com.yan.producer_demo.service.impl;
import com.yan.entity.User;
import com.yan.service.UserService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.Date;

@Service
@Component
public class UserServiceImpl implements UserService {

    @Override
    public String hello(User u){
        System.out.println(u.getName()+"---进入了sayhello,时间是"+new Date().getTime());
        System.out.println(u);
        return u.getName()+"---Hi!";
    }
}
