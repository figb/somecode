package com.yan.springrabbitmq.routing;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RoutingCustomer {

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(name = "directs",type = "direct"),
                    key = {"info","warning","error"}
            )
    })
    public void receivel1(String message){
        System.out.println("RoutingCustomer的receivel1消费了-->"+message);
    }
    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(name = "directs",type = "direct"),
                    key = "error"
            )
    })
    public void receivel2(String message){
        System.out.println("RoutingCustomer的receivel2消费了-->"+message);
    }

}
