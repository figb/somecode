package com.yan.rabbitmq.fanout;

import com.rabbitmq.client.*;
import com.yan.rabbitmq.utils.RabbitMQUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/*
    广播模式,一条消息可以被多个消费者消费
 */
public class Customer1 {


    public static void main(String[] args) throws IOException, TimeoutException {

        Connection connection = RabbitMQUtil.getConnection();
        Channel channel = connection.createChannel();
        //绑定交换机
        channel.exchangeDeclare("log","fanout");
        //临时队列
        String queue = channel.queueDeclare().getQueue();
        System.out.println("这是临时队列-->"+queue);
        //绑定交换机和临时队列
        channel.queueBind(queue,"log","");
        //消费信息
        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                super.handleDelivery(consumerTag, envelope, properties, body);
                System.out.println("fanout的消费者1消费了信息-->"+new String(body));
            }
        });
    }
}
