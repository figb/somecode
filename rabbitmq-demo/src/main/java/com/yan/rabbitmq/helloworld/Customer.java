package com.yan.rabbitmq.helloworld;

import com.rabbitmq.client.*;
import com.yan.rabbitmq.utils.RabbitMQUtil;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

/*
    简单模式，只有一个消费者
 */
public class Customer {


    @Test
    public void testGetMsg() throws IOException, TimeoutException {

    }

    public static void main(String[] args) throws IOException, TimeoutException {
//        ConnectionFactory connectionFactory = new ConnectionFactory();
//        //设置RabbitMQ服务器Host
//        connectionFactory.setHost("39.100.94.214");
//        //设置连接端口
//        connectionFactory.setPort(5672);
//        //设置连接的虚拟主机
//        connectionFactory.setVirtualHost("/helloworld");
//        //设置连接用户名
//        connectionFactory.setUsername("yan");
//        //设置用户密码
//        connectionFactory.setPassword("123456");


        Connection connection = RabbitMQUtil.getConnection();
        Channel channel = connection.createChannel();
        //这里消费者的参数和生产者的要严格一致
        channel.queueDeclare("hello",true,false,false,null);
        //消费信息
        //参数1消费的队列名称，参数2是否开启自动消息确认，参数3消费时的回调接口
        channel.basicConsume("hello",true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                super.handleDelivery(consumerTag, envelope, properties, body);这个实现方法是空的，什么都没做，我们可以重写
                System.out.println(new String(body));
            }
        });

    }
}
