package com.yan.rabbitmq.direct;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.yan.rabbitmq.utils.RabbitMQUtil;

import java.io.IOException;

/*
    direct模式，发布消息时给消息指定一个routingkey
    绑定了该routingkey的消费者才能消费该消息
    一个消息可能被n个消费者消费
 */
public class Provider {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMQUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明通道的交换机,交换机不存在时则创建一个
        channel.exchangeDeclare("log_direct",//交换机名称
                "direct"//交换机类型（direct路由模式）
        );
        String routingkey="error";
        channel.basicPublish("log_direct",routingkey,null,("This is a msg from direct，routingkey:"+routingkey).getBytes());
        RabbitMQUtil.closeConnection(connection,channel);
    }
}
