package com.yan.springrabbitmq.work;


import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class WorkCustomer {

    @RabbitListener(queuesToDeclare = @Queue(name = "work"))
    public void receivel1(String message){
        System.out.println("WorkCustomer的receivel1消费消息-->"+message);
    }

    @RabbitListener(queuesToDeclare = @Queue(name = "work"))
    public void receivel2(String message){
        System.out.println("WorkCustomer的receivel2消费消息-->"+message);
    }
}
