package com.yan.springrabbitmq;

import com.yan.springrabbitmq.pojo.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest(classes = SpringRabbitmqApplication.class )
@RunWith(SpringRunner.class)
class SpringRabbitmqApplicationTests {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    //需要注意的是，交换机和队列的创建不取决于生产者，而取决于消费者
    //也就是说如果没有消费者，无论在这里发送多少消息，交换机和队列都不会创建
    //进入队列的不一定是字符串，也可以是可序列化和反序列化的对象
    //测试之前一定要去web页面看看有没有同名的交换机和队列存在，如果有则删除旧的再测试，否则可能导致报错或新的设置不生效


    //helloworld简单模式，一个发，一个接
    @Test
    public void helloWorld(){
        rabbitTemplate.convertAndSend("hello",new User("yan","1001",new Date()));
    }


    //work工作模式，轮询或能者多劳
    @Test
    public void work(){
        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend("work","This is a msg from work--"+i);
        }
    }

    //fanout广播模式
    @Test
    public void fanout(){
        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend("log","",new User("yan","100"+i,new Date()));
        }
    }

    //routing路由模式,只有监听了对应routingKey的消费者能获取信息进行消费
    @Test
    public void routing(){
        rabbitTemplate.convertAndSend("directs","error","This is a msg from routing,routingKey is error");
    }

    //topic模式（也叫动态路由或订阅模式）,在路由模式的基础上使用通配符匹配消费者
    @Test
    public void topic(){
        rabbitTemplate.convertAndSend("topics","user.sys.hh","This is a msg from routing,routingKey is user.中文可以吗");
    }
}
