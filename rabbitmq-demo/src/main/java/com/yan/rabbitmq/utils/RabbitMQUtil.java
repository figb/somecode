package com.yan.rabbitmq.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMQUtil {
    private static ConnectionFactory connectionFactory;
    static {
        connectionFactory = new ConnectionFactory();
        //设置RabbitMQ服务器Host
        connectionFactory.setHost("39.100.94.214");
        //设置连接端口
        connectionFactory.setPort(5672);
        //设置连接的虚拟主机
        connectionFactory.setVirtualHost("/MyVirtualHost");
        //设置连接用户名
        connectionFactory.setUsername("yan");
        //设置用户密码
        connectionFactory.setPassword("123456");
    }
    public static Connection getConnection()  {
        //获取连接对象
        Connection connection = null;
        try {
            connection = connectionFactory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void closeConnection(Connection connection, Channel channel){
        try {
            if (channel!=null) channel.close();
            if (connection!=null) connection.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
