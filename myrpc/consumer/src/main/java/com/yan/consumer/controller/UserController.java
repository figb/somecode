package com.yan.consumer.controller;


import com.yan.consumer.client.FirstClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.yan.entity.User;
import com.yan.service.UserService;

import java.net.InetSocketAddress;

@RestController
public class UserController {

    private UserService proxyService = FirstClient
            .getProxyService(UserService.class, new InetSocketAddress("127.0.0.1", 10086));

    @RequestMapping("/hello")
    @ResponseBody
    public String hello(User u){
        return proxyService.hello(u);
    }


    @RequestMapping("/add")
    @ResponseBody
    public String add(String name){
        return proxyService.add(name).toString();
    }

}
