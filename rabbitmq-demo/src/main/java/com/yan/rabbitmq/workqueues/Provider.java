package com.yan.rabbitmq.workqueues;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import com.yan.rabbitmq.utils.RabbitMQUtil;

import java.io.IOException;

public class Provider {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMQUtil.getConnection();
        Channel channel = connection.createChannel();
        String str = "hell, this is a msg";
        channel.queueDeclare("work",true,false,false,null);
        for (int i = 0; i < 20; i++) {
            channel.basicPublish("","work",
                    null,//设置消息持久化(前提发送的队列设置了持久化）
                    (str+i).getBytes());
        }
        RabbitMQUtil.closeConnection(connection,channel);
    }
}
