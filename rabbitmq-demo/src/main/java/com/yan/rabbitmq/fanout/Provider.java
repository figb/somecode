package com.yan.rabbitmq.fanout;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.yan.rabbitmq.utils.RabbitMQUtil;

import java.io.IOException;

/*
    广播模式,一条消息可以被多个消费者消费
 */
public class Provider {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMQUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明通道的交换机,交换机不存在时则创建一个
        channel.exchangeDeclare("log",//交换机名称
                "fanout"//交换机类型（fanout广播）
        );
        //发送消息
        channel.basicPublish("log",//交换机名称
                "",//该参数在当前模式中没有意义，留空
                null,"This is fanout msg".getBytes());
        RabbitMQUtil.closeConnection(connection,channel);
    }

}
