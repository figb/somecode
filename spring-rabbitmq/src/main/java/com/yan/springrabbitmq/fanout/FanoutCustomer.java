package com.yan.springrabbitmq.fanout;

import com.yan.springrabbitmq.pojo.User;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class FanoutCustomer {


    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue, //这里不设置队列名，表示使用临时队列
                    exchange = @Exchange(value = "log",type = "fanout")
            )
    })
    public void receivel(User user){
        System.out.println("FanoutCustomer的receive1消费消息-->"+user);
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue, //这里不设置队列名，表示使用临时队列
                    exchange = @Exchange(value = "log",type = "fanout")
            )
    })
    public void receive2(User user){
        System.out.println("FanoutCustomer的receive2消费消息-->"+user);
    }
}
