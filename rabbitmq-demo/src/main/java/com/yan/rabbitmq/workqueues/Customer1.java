package com.yan.rabbitmq.workqueues;

import com.rabbitmq.client.*;
import com.yan.rabbitmq.utils.RabbitMQUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
/*
    work模式轮询，每个消费者平均分配
 */
public class Customer1 {


    @Test
    public void testGetMsg() throws IOException, TimeoutException {

    }

    public static void main(String[] args) throws IOException, TimeoutException {

        Connection connection = RabbitMQUtil.getConnection();
        final Channel channel = connection.createChannel();
        //这里消费者的参数和生产者的要严格一致
        channel.queueDeclare("work",true,false,false,null);
        channel.basicQos(1);//每次从队列获取一条消息，多个消费者则能者多劳
        //消费信息
        //参数1消费的队列名称，参数2是否开启自动消息确认，参数3消费时的回调接口
        channel.basicConsume("work",false,new DefaultConsumer(channel){//关闭自动确认
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                super.handleDelivery(consumerTag, envelope, properties, body);这个实现方法是空的，什么都没做，我们可以重写
                System.out.println("消费者1消费了-->"+new String(body));
                channel.basicAck(envelope.getDeliveryTag(),false);//确认消息，第二个参数为是否同时确认多个消息
            }
        });

    }
}
