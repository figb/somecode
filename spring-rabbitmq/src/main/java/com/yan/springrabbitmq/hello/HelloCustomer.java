package com.yan.springrabbitmq.hello;

import com.yan.springrabbitmq.pojo.User;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
//监听队列，如果队列不存在就创建
//@Queue指定队列名和队列设置，这里设置队列不持久化、自动删除
//如果@Queue没有设置其他参数，则默认是持久化、非独占、不自动删除
@RabbitListener(queuesToDeclare = @Queue(name = "hello",durable = "false",autoDelete = "true"))
public class HelloCustomer {

    @RabbitHandler
    public void receivel(User user){
        System.out.println("HelloCustomer消费消息-->"+user);
    }
}
