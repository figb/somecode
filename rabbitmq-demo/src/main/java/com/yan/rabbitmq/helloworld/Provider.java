package com.yan.rabbitmq.helloworld;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import com.yan.rabbitmq.utils.RabbitMQUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class Provider {

    @Test
    public void testSendMsg() throws IOException, TimeoutException {
//        ConnectionFactory connectionFactory = new ConnectionFactory();
//        //设置RabbitMQ服务器Host
//        connectionFactory.setHost("39.100.94.214");
//        //设置连接端口
//        connectionFactory.setPort(5672);
//        //设置连接的虚拟主机
//        connectionFactory.setVirtualHost("/helloworld");
//        //设置连接用户名
//        connectionFactory.setUsername("yan");
//        //设置用户密码
//        connectionFactory.setPassword("123456");

        //获取连接对象
        Connection connection = RabbitMQUtil.getConnection();
        //获取连接中的通道
        Channel channel = connection.createChannel();
        //绑定通道与队列(此处如果它绑定时找不到hello队列就会自动创建一个hello队列
        //参数1队列名，参数2是否持久化队列，参数3是否独占该队列如果true表面该队列当前连接独占
        //参数4是否消费完毕后自动删除队列，参数5额外附加参数
        channel.queueDeclare("hello",
                true,//是否持久化队列
                false,//是否让该连接独占队列（一般设置不独占）
                false,//队列中的消息全部被消费完毕后是否删除队列
                null);

        //发布消息(此处的hello才是最后发送消息的队列，一个通道是可以向多个队列发送消息的)
        channel.basicPublish("","hello",
                MessageProperties.PERSISTENT_TEXT_PLAIN,//设置消息持久化(前提发送的队列设置了持久化）
                "hell, this is a msg".getBytes());
        System.out.println(new Date().getTime());
        //关闭通道和连接
//        channel.close();
//        connection.close();
        RabbitMQUtil.closeConnection(connection,channel);
    }
}
