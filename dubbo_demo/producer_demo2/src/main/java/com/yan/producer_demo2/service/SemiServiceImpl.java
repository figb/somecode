package com.yan.producer_demo2.service;
import com.yan.entity.User;
import com.yan.service.SemiService;
import com.yan.service.UserService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.Date;


@Service
@Component
public class SemiServiceImpl implements SemiService {
    @Reference
    private UserService userService;

    @Override
    public String Login(String name) {
        System.out.println(name+"--->登录了，远调sayhello");
        String hello = userService.hello(new User(name, 0, new Date()));
        return hello;
    }



}
