package com.yan.producer_demo2;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class ProducerDemo2Application {

    public static void main(String[] args) {
        SpringApplication.run(ProducerDemo2Application.class, args);
    }

}
