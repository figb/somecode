package com.yan.rabbitmq.direct;

import com.rabbitmq.client.*;
import com.yan.rabbitmq.utils.RabbitMQUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/*
    direct模式，发布消息时给消息指定一个routingkey
    绑定了该routingkey的消费者才能消费该消息
    一个消息可能被n个消费者消费
 */
public class Customer2 {


    public static void main(String[] args) throws IOException, TimeoutException {

        Connection connection = RabbitMQUtil.getConnection();
        Channel channel = connection.createChannel();
        //绑定交换机
        channel.exchangeDeclare("log_direct","direct");
        //临时队列
        String queue = channel.queueDeclare().getQueue();
        System.out.println("这是临时队列-->"+queue);
        //绑定交换机和临时队列，这里绑定多个routingkey
        channel.queueBind(queue,"log_direct","error");
        channel.queueBind(queue,"log_direct","info");
        channel.queueBind(queue,"log_direct","warning");
        //消费信息
        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                super.handleDelivery(consumerTag, envelope, properties, body);
                System.out.println("direct的消费者1消费了信息-->"+new String(body));
            }
        });
    }
}
