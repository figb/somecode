package com.yan.springrabbitmq.topic;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class TopicCustomer {

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(name = "topics",type = "topic"),
                    key = {"user.#"}//#匹配0或多个词
            )
    })
    public void receivel1(String message){
        System.out.println("TopicCustomer的receivel1消费了-->"+message);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue,
                    exchange = @Exchange(name = "topics",type = "topic"),
                    key = {"user.*"}//*匹配0或1个词
            )
    })
    public void receivel2(String message){
        System.out.println("TopicCustomer的receivel2消费了-->"+message);
    }

}
