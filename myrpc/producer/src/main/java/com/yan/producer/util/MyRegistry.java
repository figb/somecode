package com.yan.producer.util;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class MyRegistry {

    //存放可供使用的接口实现类，key为接口名，value为接口的实现类Class对象
    private static HashMap<String,Class> someServices=new HashMap<>();



    public void Listen() throws Exception{
        ServerSocket ss = new ServerSocket(10086);
        Socket accept = null;
        String s = new String();
        while (true){//死循环，不然只能调用一次
            accept = ss.accept();//监听会话，accept()为阻塞方法，监听到会话才会往下执行
            //这里应该用线程池的，测一下没问题再说
            Thread thread = new Thread(new ServerThread(accept, someServices));
            thread.start();//为监听到的会话创建一个线程执行，执行完毕即销毁

        }
    }

    private class ServerThread implements Runnable{//为了多次调用而创建的多线程类，内部类
        Socket socket = null;
        HashMap<String,Class> servers=null;
        public ServerThread(Socket s,HashMap<String,Class> servers) {
            this.servers=servers;
            this.socket=s;
        }
        @Override
        public void run() {//根据客户端的调用，执行，每执行完一次调用即销毁
            ObjectInputStream objectInputStream = null;
            ObjectOutputStream objectOutputStream=null;
            try {
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                String interfaceName = objectInputStream.readUTF();//客户端想调用的接口
                String methodName = objectInputStream.readUTF();//客户端想要调用的方法名
                Class[] objs =(Class[]) objectInputStream.readObject();//客户端给的方法参数数据类型
                Object[] args =(Object[]) objectInputStream.readObject();//客户端给的方法参数
                Class service = someServices.get(interfaceName);//获取接口实现类的Class对象
                if (service==null){
                    throw new Exception("Service not found");
                }
                Method method = service.getMethod(methodName, objs);//获取目标方法对象
                Object re = method.invoke(service.newInstance(), args);//执行目标方法
                //将执行获得的返回值响应给客户端
                objectOutputStream  = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject(re);//给客户端执行方法的返回值
            } catch (Exception e) {
                e.printStackTrace();
            }

            //关闭资源
            try {
                if (socket!=null){
                    socket.close();
                }
                if(objectOutputStream!=null){
                    objectOutputStream.close();
                }
                if (objectInputStream!=null){
                    objectInputStream.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }


    //根据服务名获取接口Class对象
    public static Class getService(String serviceName){
        return someServices.get(serviceName);
    }

    public static void putService(Class interfaceClass,Class interfaceImpl){
        someServices.put(interfaceClass.getName(),interfaceImpl);
    }

}
