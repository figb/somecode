package com.yan.client_demo.controller;


import com.yan.entity.User;
import com.yan.service.SemiService;
import com.yan.service.UserService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserController {

    @Reference
    private SemiService semiService;

    @RequestMapping("/hello")
    public String hello(String name){
        return semiService.Login(name);
    }

}
