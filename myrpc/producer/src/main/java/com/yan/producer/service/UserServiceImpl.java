package com.yan.producer.service;



import org.springframework.stereotype.Component;
import com.yan.entity.User;
import com.yan.service.UserService;

import java.util.Date;


@Component
public class UserServiceImpl implements UserService {

    @Override
    public String hello(User u){
        System.out.println(u.getName()+"in producer.hello,time: "+new Date().getTime());
        return u.getName()+"---Hi!";
    }

    @Override
    public String  add(String name) {
        System.out.println("in producer.add,time: "+new Date().getTime());
        return new User(name,0,new Date()).toString();
    }


}
