package com.yan.consumer.client;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;

public class FirstClient {
    public static <T> T getProxyService(Class service, InetSocketAddress inetSocketAddress){
        InvocationHandler invocationHandler = new InvocationHandler() {
            //直接用匿名类
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Object o=null;
                //利用代理将方法调用替换为socket通信远程调用
                Socket socket = null;
                ObjectOutputStream objectOutputStream = null;
                ObjectInputStream objectInputStream = null;
                try {
                    socket = new Socket();
                    socket.connect(inetSocketAddress);
                    objectOutputStream = new ObjectOutputStream(socket.getOutputStream());//输出流，用作向服务提供者发送信息
                    //需要发什么信息？ 接口名，方法名，方法参数
                    String interfaceName = service.getName();//接口名
                    String methodName = method.getName();//方法名
                    Object[] objs = method.getParameterTypes();//方法参数的数据类型也要发过去，不然没法反射调用
                    objectOutputStream.writeUTF(interfaceName);//发送接口名称
                    objectOutputStream.writeUTF(methodName);//发送调用的方法名
                    objectOutputStream.writeObject(objs);//发送参数的数据类型数组
                    objectOutputStream.writeObject(args);//发送方法参数
                    //发完了，等待服务端响应
                    objectInputStream = new ObjectInputStream(socket.getInputStream());
                    o = objectInputStream.readObject();//接收客户端返回的执行结果（即方法返回值）
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    if(socket!=null){
                        socket.close();
                    }
                    if (objectInputStream!=null){
                        objectInputStream.close();
                    }
                    if (objectOutputStream!=null){
                        objectOutputStream.close();
                    }
                }
                return o;//将那边返回的结果return
            }
        };
        //因为并不确定会调用哪个，所以使用泛型
        return  (T) Proxy.newProxyInstance(service.getClassLoader(),new Class[]{service},invocationHandler);
    }
}